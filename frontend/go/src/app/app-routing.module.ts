import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from "./auth.guard";
//components 
import {  HomeComponent} from "./components/home/home.component";
import {SignupComponent} from './components/signup/signup.component';
import {SigninComponent}from './components/signin/signin.component';
import {MoviesComponent} from './components/movies/movies.component';
import { PrivateMoviesComponent } from "./components/private-movies/private-movies.component";
import { SuperAdminComponent} from "./components/super-admin/super-admin.component";
import { AdminComponent } from "./components/admin/admin.component";
import { SearchComponent } from './components/search/search.component';
import { InterfazMoviesComponent } from "./components/interfaz-movies/interfaz-movies.component";
import {  ReyleonComponent} from "./trailers/reyleon/reyleon.component";
import { DeadpoolComponent } from "./trailers/deadpool/deadpool.component";
import { GuasonComponent } from "./trailers/guason/guason.component";
import {  ItComponent} from "./trailers/it/it.component";
import { ParasitoComponent } from "./trailers/parasito/parasito.component";
import { QueenComponent } from "./trailers/queen/queen.component";
import { MilNovecientosComponent } from "./peliculas/mil-novecientos/mil-novecientos.component";
const routes: Routes = [
  {
    path:'',
    redirectTo:'/home',
    pathMatch: 'full'
  },
  {
    path: 'home', 
    component: HomeComponent
  },
  {
    path: 'search/:name', 
    component: SearchComponent
  },
  {
    path:'premium',
    component:PrivateMoviesComponent,
    canActivate: [AuthGuard]
  }, 
  
  {
    path:'interMovie',
    component:InterfazMoviesComponent,
    canActivate: [AuthGuard]
  }, 
  {
    path:'signup',
    component:SignupComponent,
  },
  {
    path:'signin',
    component: SigninComponent
  },
  {
    path:'superAdmin',
    component: SuperAdminComponent
  },
  {
    path:'admin',
    component: AdminComponent
  }, 
  {
    path: 'movies',
    component: MoviesComponent
  },
  {
    path:'reyLeon',
    component: ReyleonComponent
  }, 
  {
    path:'deadpool',
    component: DeadpoolComponent
  }, 
  {
    path:'guason',
    component: GuasonComponent
  }, 
  {
    path:'it',
    component: ItComponent
  }, 
  {
    path:'parasito',
    component: ParasitoComponent
  }, 
  {
    path:'queen',
    component: QueenComponent
  }, 
  {
    path:'1917',
    component: MilNovecientosComponent
  }, 
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
