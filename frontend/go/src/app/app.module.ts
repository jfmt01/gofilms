import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthGuard } from "./auth.guard";
import { ReactiveFormsModule } from '@angular/forms';
import { NgxStripeModule } from 'ngx-stripe';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './components/signup/signup.component';
import { SigninComponent } from './components/signin/signin.component';
import { MoviesComponent } from './components/movies/movies.component';
import { PrivateMoviesComponent } from './components/private-movies/private-movies.component';
import { HttpClient } from 'selenium-webdriver/http';



import { TokenInterceptorService } from "./service/token-interceptor.service";
import { SuperAdminComponent } from './components/super-admin/super-admin.component';
import { AdminComponent } from './components/admin/admin.component';
import { HomeComponent } from './components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MoviesListComponent } from './components/movies-list/movies-list.component';
import { ModalComponent } from './components/modal/modal.component';
import { SearchComponent } from './components/search/search.component';
import { MaterialModule } from './material.module';
import { MatDialogModule } from '@angular/material/dialog';
import { OverlayModule } from "@angular/cdk/overlay";
import { InterfazMoviesComponent } from './components/interfaz-movies/interfaz-movies.component';
import { ReyleonComponent } from './trailers/reyleon/reyleon.component';
import { DeadpoolComponent } from './trailers/deadpool/deadpool.component';
import { GuasonComponent } from './trailers/guason/guason.component';
import { ItComponent } from './trailers/it/it.component';
import { ParasitoComponent } from './trailers/parasito/parasito.component';
import { QueenComponent } from './trailers/queen/queen.component';
import { MilNovecientosComponent } from './peliculas/mil-novecientos/mil-novecientos.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    MoviesComponent,
    PrivateMoviesComponent,
    SuperAdminComponent,
    AdminComponent,
    HomeComponent,
    MoviesListComponent,
    ModalComponent,
    SearchComponent,
    InterfazMoviesComponent,
    ReyleonComponent,
    DeadpoolComponent,
    GuasonComponent,
    ItComponent,
    ParasitoComponent,
    QueenComponent,
    MilNovecientosComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule, 
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxStripeModule.forRoot('pk_test_kkVKlC7CeU0IIMTHeibiLiU100W7cQfX36'),
    BrowserAnimationsModule,
    MaterialModule,
    OverlayModule,
    MatDialogModule
   
    
  ],
  entryComponents:[ModalComponent],
  providers: [ MatDialogModule, AuthGuard, 
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi:true 
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
