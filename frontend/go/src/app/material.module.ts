import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule, MatIconModule, MatButtonModule, MatFormFieldModule, MatInputModule,  } from "@angular/material";
import { OverlayModule } from "@angular/cdk/overlay";


@NgModule({
  declarations: [],
  exports:[
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    OverlayModule,
    MatFormFieldModule,
    MatInputModule
  
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    OverlayModule,
    MatFormFieldModule,
    MatInputModule
   
    
  ]
})
export class MaterialModule { }
