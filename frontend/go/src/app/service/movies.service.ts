import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Movie } from "../models/movie";

@Injectable({
  providedIn: 'root'
})

export class MoviesService {
  private movie: Movie;
  private url: String = 'http://localhost:3500/api';
  private headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  createMovie(movie: Movie) {
    return this.http.post(this.url + '/create', movie, { headers: this.headers });
  }

  readMovie() {
    return this.http.get(this.url + '/read', { headers: this.headers });
  }

  updateMovie(movie:Movie){
    return this.http.put(this.url+'/update', movie, {headers: this.headers});
  }

  deleteMovie(id:String){
    return this.http.delete(this.url+'/delete/'+ id, {headers: this.headers});
  }

  setter(movie:Movie){
    this.movie = movie
  }

  getter(){
    return this.movie;
  }
}
