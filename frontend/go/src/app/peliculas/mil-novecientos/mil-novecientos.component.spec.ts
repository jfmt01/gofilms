import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MilNovecientosComponent } from './mil-novecientos.component';

describe('MilNovecientosComponent', () => {
  let component: MilNovecientosComponent;
  let fixture: ComponentFixture<MilNovecientosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MilNovecientosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilNovecientosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
