
    export class Movie {
        public _id?: String;
        public nombre: String;
        public protagonistas: String[];
        public director: String;
        public duracion: String;
        public genero: String;
        public imagen: String;
        public archivo: String
    }

