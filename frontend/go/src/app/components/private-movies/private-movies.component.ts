
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StripeService, Elements, Element as StripeElements, ElementOptions } from 'ngx-stripe';
import { Router } from "@angular/router";


@Component({
  selector: 'app-private-movies',
  templateUrl: './private-movies.component.html',
  styleUrls: ['./private-movies.component.css']
})
export class PrivateMoviesComponent implements OnInit {
    elements: Elements;
    card: StripeElements;
    
  elementOptions:ElementOptions = {
    
  }

  stripeTest: FormGroup
  
  constructor( 
    private  fb: FormBuilder,
    private stripeService: StripeService,
    private router: Router
    ) {}
  
  
 
  
 
  ngOnInit() {
    this.stripeTest= this.fb.group({
      name:['', Validators.required]
    })
    this.stripeService.elements().subscribe(
      elements =>{
        this.elements= elements;
        if(!this.card){
          this.card= this.elements.create('card', {
            style:{
              base:{
                iconColor: 'rgb(19, 96, 219)',
                color: '#fff',
                lineHeight: '40px',
                fontWeight: 300,
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSize: '20px',
              }
            }
          });
          this.card.mount('#card-element')
        }
      }
    )
  }
  buy(){
    const name= this.stripeTest.get('name').value;
    this.stripeService.createToken(this.card, {name}).subscribe(
      result=>{
        if(result.token){
          console.log('token', result.token),
          this.router.navigate(['/interMovie'])
        }else if ( result.error){
         console.log('error', result.error.message)
        }
      }
    )
  }
}
