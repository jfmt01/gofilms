import { Component, OnInit } from '@angular/core';
import { importExpr, THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AuthService } from "../../service/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

user= {
  name:'',
  lastName:'',
  email:'',
  password:'',
  confirmPassword:''
}

  constructor(
    private AuthService: AuthService,
    private router:Router
    
    ) { }

  ngOnInit() {
  }
  signUp(){
   this.AuthService.signUp(this.user)
   .subscribe(
     res=>{
      console.log(res)
      localStorage.setItem('token', res.token)
      this.router.navigate(['/premium'])
     },
     err=>{
      console.log(err)
     }
   )
}
}
