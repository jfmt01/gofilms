import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterfazMoviesComponent } from './interfaz-movies.component';

describe('InterfazMoviesComponent', () => {
  let component: InterfazMoviesComponent;
  let fixture: ComponentFixture<InterfazMoviesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfazMoviesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfazMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
