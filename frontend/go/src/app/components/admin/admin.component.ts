import { Component, OnInit } from '@angular/core';
import { Router, Routes } from "@angular/router";
import {  MoviesService} from "../../service/movies.service";
import { Movie } from "../../models/movie";
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  public movie: Movie;
  public subirImg: File;
  constructor(
    private movieService: MoviesService,
    private router: Router) { }

  ngOnInit(): void {
    this.movie = this.movieService.getter();
  }

  subirArchivo(fileInput: any) {
    this.subirImg = <File>fileInput.target.files[0];
    console.log(this.subirImg)
  }

  createOrUpdate() {
    if (this.movie._id == null) {
      this.movieService.createMovie(this.movie).subscribe(
        data => {
          console.log(data);
          this.router.navigate(['/'])
        }, error => {
          console.log(error);
        }
      )
    }else{
      this.movieService.updateMovie(this.movie).subscribe(
        data => {
          console.log(data);
          this.router.navigate(['/'])
        }, error => {
          console.log(error);
        }
      )
    }
  }

}
