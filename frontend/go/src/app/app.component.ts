import { Component } from '@angular/core';
import { AuthService } from "./service/auth.service";
import { MatDialog, MatDialogModule, MatDialogRef,  } from '@angular/material/dialog';
import { ModalComponent } from './components/modal/modal.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(
    private authService: AuthService,
    private dialog: MatDialog,
    
    ) {}
   OnInit(){
   
    }
    openDialog():void{
      const dialogRef= this.dialog.openDialog(ModalComponent, {
        width: '250px',
      })
   dialogRef.afterClosed().subscribe(res=>{
     console.log(res)
   })
    }
  
}
