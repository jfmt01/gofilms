import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParasitoComponent } from './parasito.component';

describe('ParasitoComponent', () => {
  let component: ParasitoComponent;
  let fixture: ComponentFixture<ParasitoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParasitoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParasitoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
