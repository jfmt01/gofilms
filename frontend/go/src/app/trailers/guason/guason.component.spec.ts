import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuasonComponent } from './guason.component';

describe('GuasonComponent', () => {
  let component: GuasonComponent;
  let fixture: ComponentFixture<GuasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
