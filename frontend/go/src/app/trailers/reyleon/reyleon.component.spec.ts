import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReyleonComponent } from './reyleon.component';

describe('ReyleonComponent', () => {
  let component: ReyleonComponent;
  let fixture: ComponentFixture<ReyleonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReyleonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReyleonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
