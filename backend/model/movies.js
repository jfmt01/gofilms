const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let movieSchema = Schema({
    nombre:String,
    protagonistas : Array,
    director :String,
    duracion :String,
    genero : String,
    imagen :String,
    archivo :String
});

module.exports = mongoose.model('movie', movieSchema);