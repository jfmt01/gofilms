const express= require('express');
const app=express();
const bodyParser= require('body-parser');
const multipart=require('connect-multiparty');
const cors=require('cors');


// middleware


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true 
}))


require('./database');
app.use(express.json());

app.use(cors())   
app.use('/api', require('./routes/user'));
app.use('/api', require('./routes/movies'));
app.use('/api', require('./routes/admin'));


app.listen(3500);
console.log('server on port', 3500);