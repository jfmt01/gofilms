const router= require('express').Router();
const jwt =require('jsonwebtoken');
Movie = require('../model/movies');




router.get('/movies', (req,res)=>{
    res.json([
        {
            id:"1",
            name:"test"
        }
    ])
});

router.get('/private-movies',verifyToken, (req, res)=>{
    res.json([
        {
            id:"1",
            name:"test"
        }
    ])
})

//module.exports= router

function verifyToken(req, res, next){
  if(!req.headers.authorization){
      return res.status(401).send('no eres premium')
  }
 const token= req.headers.authorization.split(' ')[1]
 if(token ===null){
    return res.status(401).send('no eres premium')
 }

 const payload= jwt.verify(token, 'secretKey')
 req.userId=payload._id
  console.log(payload)
  next();
}



   

router.post('/create', (req, res, next) => {
    let propiedades = req.body;

    let movie = new Movie({
        nombre: propiedades.nombre,
        protagonistas: propiedades.protagonistas,
        director: propiedades.director,
        duracion: propiedades.duracion,
        genero: propiedades.genero,
        imagen: null,
        archivo: null
    });
    movie.save((err, newMovie) => {
        if (err) {
            res.status(500).json({ errmsg: err });
        }
        res.status(200).json({ movie: newMovie });

    });
});

router.get('/read', (req, res, next) => {
    Movie.find({}, (err, movies) => {
        if (err) {
            res.status(500).json({ errmsg: err });
        }
        res.status(200).json({ msg: movies });
    });
});

router.put('/update', (req, res, next) => {
    Movie.findById(req.body._id, (err, movie) => {
        if (err) {
            res.status(500).json({ errmsg: err });
        } else {
            let propiedades = req.body;
            movie.nombre= propiedades.nombre;
            movie.protagonistas= propiedades.protagonistas,
            movie.director= propiedades.director;
            movie.duracion= propiedades.duracion;
            movie.genero= propiedades.genero;
            movie.save((err, updateMovie) => {
                if (err) {
                    res.status(500).json({ errmsg: err });
                }
                res.status(200).json({ movie: updateMovie });
            })
        }
    })

});

router.delete('/delete/:id', (req, res, next) => {
    Movie.findOneAndRemove({ _id: req.params.id }, (err, deleteMovie) => {
        if (err) {
            res.status(500).json({ errmsg: err });
        }
        res.status(200).json({movie: deleteMovie });
    })

});

module.exports = router;