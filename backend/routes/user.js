const{Router}=require('express');
const User= require('../model/User')
const router= Router();
const jwt= require('jsonwebtoken');

router.get('/',(req, res) =>{
    res.send('home');
});

router.post('/signup', async (req, res)=>{
    const{name, email, password}= req.body;
    newUser= new User({
        name, 
        email, 
        password
     })
    await newUser.save();


   const token= jwt.sign({_id: newUser._id}, 'keySecret');
   res.status(200).json({token: token})
    
    console.log(newUser);
    res.send('signup')

});

router.post('/signin',async (req, res)=>{
    const{email, password}= req.body;
   const user= await User.findOne({email })
   if(!user) return res.status(401).send('User not font')
   if(user.password !== password) return res.status(401).send('wrong password')

   const token= jwt.sign({_id: user._id}, 'secretKey')
   return  res.status(200).json({token})
})

module.exports=router
